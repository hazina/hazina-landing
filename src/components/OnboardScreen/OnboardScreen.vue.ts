import { Component, Prop, Vue } from "vue-property-decorator";
import * as _ from "lodash";

@Component
export default class OnboardScreen extends Vue {

    @Prop() private title!: string;
    @Prop() private subtitle!: string;
    @Prop() private image!: string;
    @Prop() private route!: number;
    @Prop() private buttonText!: number;

    // acl_routes = [
    //     {route: "OnboardAccountUpdate", text: "Setup Account", key: 1},
    //     {route: "OnBoardingSurvey", text: "Take Quick Survey", key: 2}
    // ]



}
