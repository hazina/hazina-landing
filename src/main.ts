import Vue from "vue";
import VueGtm from 'vue-gtm';
import App from "./views/App/App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Axios from "axios";
import Vuelidate from "vuelidate";
import Toasted from "vue-toasted";
//const VueCountdownTimer = require("vuejs-countdown-timer")
//const VueTelInputVuetify = require("vue-tel-input-vuetify");
import VueTelInputVuetify from "vue-tel-input-vuetify/lib";
import VueMeta from 'vue-meta'
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: 'AIzaSyCryox6xfSrAJRW_PtbEXLc70c_mLaUB6M', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client:
});

Vue.use(VueGtm, {
  id: 'GTM-NPDFLJK', // Your GTM single container ID or array of container ids ['GTM-xxxxxx', 'GTM-yyyyyy']
  // queryParams: { // Add url query string when load gtm.js with GTM ID (optional)
  //   gtm_auth:'AB7cDEf3GHIjkl-MnOP8qr',
  //   gtm_preview:'env-4',
  //   gtm_cookies_win:'x'
  // },
  defer: false, // defaults to false. Script can be set to `defer` to increase page-load-time at the cost of less accurate results (in case visitor leaves before script is loaded, which is unlikely but possible)
  enabled: true, // defaults to true. Plugin can be disabled by setting this to false for Ex: enabled: !!GDPR_Cookie (optional)
  debug: false, // Whether or not display console logs debugs (optional)
  //loadScript: true, // Whether or not to load the GTM Script (Helpful if you are including GTM manually, but need the dataLayer functionality in your components) (optional)
  vueRouter: router, // Pass the router instance to automatically sync with router (optional)
  //ignoredViews: ['homepage'], // Don't trigger events for specified router names (case insensitive) (optional)
  trackOnNextTick: true, // Whether or not call trackView in Vue.nextTick
});

Vue.config.productionTip = true
Vue.use(vuetify);
Vue.use(VueTelInputVuetify, {
    vuetify,
});

Vue.use(Vuelidate);
Vue.use(Toasted);
Vue.use(VueMeta)

// Vue.use(VueCountdownTimer);

Vue.prototype.$http = Axios;
const token = localStorage.getItem('haziAuth')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+token
}
Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
//Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.gethazina.com/v1' : 'https://hazinang.dev/v1';
Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.gethazina.com/v1' : 'https://hazinang.dev/v1';
// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
// Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.hazina.xyz/v1' : 'https://hazinang.dev/v1';

// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer amfmw136hC1RjhdZCamMeiN8x349utATvrcXWJo8vfUQRKHYnGGxJQLxQ9JI';
// Vue.prototype.$http.defaults.baseURL = 'https://hazinang.dev/v1';

new Vue({
  router,
  store,  
  vuetify,
  render: h => h(App)
}).$mount("#app");
