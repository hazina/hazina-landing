import Vue from 'vue'
import VueRouter, { RouteConfig } from "vue-router";
import store from '../store'
import Home from '../views/Home/Home.vue'
import WithHeader from "../views/Layouts/WithHeader.vue"
import NoHeader from "../views/Layouts/NoHeader.vue"
import Offer from "../views/Layouts/Offer.vue"
import Consumer from "../views/Layouts/Consumer/Consumer.vue"
import Thanks from '@/views/Thanks/Thanks.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    {
        path: '/',
        component: WithHeader,
        children: [
            {
                path: '',
                name: 'Home',
                component: Home
            },
            {
                path: 'login',
                name: 'Login',
                component: ()=> import('../views/Login/Login.vue')
            },
            {
                path: 'terms',
                name: 'Terms',
                component: ()=> import('../views/Terms/Terms.vue')
            },
            {
                path: 'privacy',
                name: 'Privacy',
                component: ()=> import('../views/Privacy/Privacy.vue')
            },
            {
                path: 'register',
                name: 'Register',
                component: ()=> import('../views/Register/Register.vue')
            },
        
            {
                path: 'forgot-pasword',
                name: 'ForgotPassword',
                component: ()=> import('../views/ForgotPassword/ForgotPassword.vue')
            },
            {
                path: 'reset-password/:token',
                name: 'ResetPassword',
                component: ()=> import('../views/ResetPassword/ResetPassword.vue')
            },
            {
                path: 'verify/phone/:id',
                name: 'VerifyPhone',
                component: ()=> import('../views/VerifyPhone/VerifyPhone.vue')
            },
            {
                path: 'phone-code/verify/:code',
                name: 'VerifyPhoneCode',
                component: ()=> import('../views/PhoneCodeVerify/PhoneCodeVerify.vue')
            },
            {
                path: 'account/verify/:token',
                name: 'VerifyEmail',
                component: ()=> import('../views/VerifyEmail/VerifyEmail.vue')
            },
            {
                path: 'user/verify-email/:token',
                name: 'UserVerifyEmail',
                component: ()=> import('../views/UserVerifyEmail/UserVerifyEmail.vue')
            },
            {
                path: 'welcome/setup/:token',
                name: 'ClientCreatePasswordUnverifiedEmail',
                component: ()=> import('../views/ClientCreatePasswordUnverifiedEmail/ClientCreatePasswordUnverifiedEmail.vue')
            },
            {
                path: 'welcome/account/setup/:id',
                name: 'ClientCreatePasswordVerifiedEmail',
                component: ()=> import('../views/ClientCreatePasswordVerifiedEmail/ClientCreatePasswordVerifiedEmail.vue')
            },
            {
                path: 'email/resend-verification',
                name: 'ResendVerification',
                component: ()=> import('../views/ResendVerification/ResendVerification.vue')
            },
        ]
    },
    {
        path: '/thank-you',
        component: NoHeader,
        children: [
            {
                path: '',
                name: 'Thanks',
                component: Thanks
            },
        ]
    },
    {
        path: '/campaign/:id/overview',
        component: Offer,
        children: [
            {
                path: '',
                name: 'OfferPage',
                component:()=>import("../views/Offer/Offer.vue") 
            },
        ]
    },

]

const router = new VueRouter({
    mode: 'history',
    //base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
});

export default router
