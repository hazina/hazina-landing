import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('haziAuth') || '',
    user : {},
    location: null,
    loginUrl: (process.env.NODE_ENV === "production") ? "https://consumer.gethazina.com/auth/" : "http://localhost:8081/auth/",
    // loginUrl: (process.env.NODE_ENV === "production") ? "https://consumer.hazina.xyz/auth/" : "http://localhost:8081/auth/",
  },
  mutations: {
    auth_request(state){
        state.status = 'loading'
      },
    auth_success(state, token){
    state.status = 'success'
    state.token = token
    //state.user = user
    },
    auth_error(state){
    state.status = 'error'
    },
    logout(state){
    state.status = ''
    state.token = ''
    },
    set_user(state, user){
    state.user = user
    },
    verify_referrer(state, location){
      state.location = location
    }
  },
  actions: {
    login({commit}, user){
        return new Promise((resolve, reject) => {
          commit('auth_request')
          axios({url: '/consumer/auth/login', data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user
            localStorage.setItem('haziAuth', token)
            axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', token)
            commit('set_user', user)
            resolve(resp)
          })
          .catch(err => {
            commit('auth_error')
            localStorage.removeItem('haziAuth')
            reject(err) 
          })
        })
    },
    token_login({commit}, token){
        return new Promise((resolve, reject) => {
            commit('auth_success', token)
            localStorage.setItem('haziAuth', token)
            axios.defaults.headers.common['Authorization'] = token
            resolve(token)
        })
    },
    logout({commit}){
        return new Promise((resolve, reject) => {
          commit('logout')
          localStorage.removeItem('haziAuth')
          delete axios.defaults.headers.common['Authorization']
         // resolve()
        })
    },
  },
  modules: {
  },
  getters:{
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    user: state => state.user,
  }
})
