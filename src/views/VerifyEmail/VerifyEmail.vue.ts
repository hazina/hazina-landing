import { Component, Prop, Vue } from "vue-property-decorator";
import axios from 'axios';
import Checkmark from '../Checkmark/Checkmark.vue';
import Spinner from '../Spinner/Spinner.vue';
@Component({
    components:{
        Checkmark,
        Spinner
    }
})
export default class VerifyEmail extends Vue {
    result = "";
    emailMsg = "";
    hasError = false;
    isSuccess = false;
    isVerified = false;

    verifyEmail() {
        let data = {
            email_token: this.$route.params.token
        };
        let that = this;

        axios
            .post("/consumer/auth/verify-email", data)
            .then(res => {
                this.hasError = false;
                this.isSuccess = true;
                this.isVerified = true;
                this.result = "success";
            })
            .catch(err => {
                this.hasError = true;
                this.isSuccess = false;
                this.isVerified = true;
                this.emailMsg = err.response.data.errors;
            })
            .finally(function() {
                //that.isLoading = false;
            });
    }

    mounted() {
        this.verifyEmail();
    }
}
