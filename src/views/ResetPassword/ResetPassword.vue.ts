import { Component, Prop, Vue } from 'vue-property-decorator';
import { required, minLength, email, sameAs } from "vuelidate/lib/validators";
import axios from 'axios';
import * as _ from "lodash";

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        confirm_password: { required, minLength: minLength(6), sameAs: sameAs('password') },
        email: { required, email }
    }
})
export default class ResetPassword extends Vue {
    isLoading: boolean = false;
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";

    email: string = "";
    password: string = "";
    confirm_password: string = "";
    submit(){
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        }else{
            let data = {
            email: this.email,
            password: this.password,
            token: this.$route.params.token
            };
            let that = this;

            axios
            .post("/consumer/auth/reset-password", data)
            .then((res)=>{
                //console.log(res);
                this.isLoading = false;
                this.hasError = false;
                this.showAlert = true; 
                this.$toasted.success("Your password has been changed successfully!").goAway(1000);
                this.$router.push({name: 'Login'});
            } )
            .catch((err)=>{
                this.hasError = true;
                this.showAlert = true; 
                if (_.has(err.response.data, "errors")) {
                    var field = _.keys(err.response.data.errors);
                    this.errorMessages = err.response.data.errors[field[0]][0];
                } else {
                    this.errorMessages = err.response.data.message;
                }
                this.isLoading = false;
            })
            .finally(function () {
               that.isLoading = false;
            });
        }

    }

    get emailErrors () {
        const errors: Array<string> = []
        if (!this.$v.email.$dirty) return errors
        !this.$v.email.email && errors.push('Must be valid e-mail')
        !this.$v.email.required && errors.push('E-mail is required')
        return errors
      }
      get passwordErrors () {
        const errors: Array<string> = []
        if (!this.$v.password.$dirty) return errors
        !this.$v.password.required && errors.push('Password is required')
        !this.$v.password.minLength && errors.push('Password must be at least 4 characters long')
        //!this.$v.password.sameAs && errors.push('Passwords do not match')
        return errors
      }
      get confimPasswordErrors () {
        const errors: Array<string> = []
        if (!this.$v.confirm_password.$dirty) return errors
        !this.$v.confirm_password.required && errors.push('You must confirm your password.')
        !this.$v.confirm_password.minLength && errors.push('Password must be at least 4 characters long')
        !this.$v.confirm_password.sameAs && errors.push('Passwords do not match')
        return errors
      }

}
