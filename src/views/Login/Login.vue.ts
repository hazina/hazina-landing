import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, email } from "vuelidate/lib/validators";
import store from "../../store";
import axios from "axios";

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        email: { required, email }
    }
})
export default class Login extends Vue {
    isLoading: boolean = false;
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";

    email: string = "";
    password: string = "";
    //loginRedirectUrl: string = "https://consumer.gethazina.com";

    submit() {
        store.state.loginUrl
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let data = {
                email: this.email,
                password: this.password
            }; 
            let that = this;
            store
                .dispatch("login", data)
                .then((resp: any) => {
                    if(resp.data.id){
                        this.hasError = true;
                        this.errorMessages = resp.data.message;
                        this.isLoading = false;
                        this.showAlert = true; 
                        if(resp.data.type == 'ph'){
                            this.$toasted.error(resp.data.message).goAway(5000);
                            this.$router.push({name: 'VerifyPhone', params:{id: resp.data.id}});
                        }else if(resp.data.type == 'em'){ 
                            this.$toasted.error(resp.data.message).goAway(5000);
                            this.$router.push({name: 'ResendVerification'});
                        }
                    }else{ 
                        this.$toasted 
                        .success("You have logged in successfully!")
                        .goAway(1500);
                        // window.location.href = "/portal/consumer";
                        //this.$router.push({name: 'ConsumerHome'});
                        
                        window.location.href = store.state.loginUrl+resp.data.user_id;
                    } 
                   // this.fetchUser();
                    // this.$toasted
                    // .success("You have logged in successfully!")
                    // .goAway(1500);
                    // window.location.href = "/portal/consumer";
                    // //this.$router.push("/");

                })
                .catch(err => {
                    this.hasError = true;
                    this.showAlert = true;
                    this.errorMessages = err.response.data.errors;
                    this.isLoading = false;
                })

        }
    }

    fetchUser() {
        axios
            .get("/api/user/profile")
            .then(res => {
                store.commit("set_user", res.data.user);
                this.isLoading = false;
                this.hasError = false;
            })
            .catch(error => {
                this.$toasted.error("Unable to fetch user").goAway(1000);
            });
    }

    get emailErrors() {
        var errors: Array<string> = [];
        if (!this.$v.email.$dirty) return errors;
        !this.$v.email.email && errors.push("Must be valid e-mail");
        !this.$v.email.required && errors.push("E-mail is required");
        return errors;
    }
    get passwordErrors() {
        var errors: Array<string> = [];
        if (!this.$v.password.$dirty) return errors;
        !this.$v.password.required && errors.push("Password is required");
        !this.$v.password.minLength &&
            errors.push("Password must be at least 6 characters long");
        return errors;
    }
}
