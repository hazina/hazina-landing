import { Component, Prop, Vue } from "vue-property-decorator";
import { required, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
    validations: {
        email: { required, email }
    }
})
export default class ForgotPassword extends Vue {
    isLoading: boolean = false;
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";

    email: string = "";

    submit() {
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let data = {
                email: this.email
            };
            let that = this;

            axios
                .post("/consumer/auth/forgot-password", data)
                .then(res => {
                    // console.log(res);
                    this.isLoading = false;
                    // this.isSuccess = true;
                    this.hasError = false;
                    this.showAlert = true; 
                    this.$toasted.success("A password reset link has been sent to your email address").goAway(10000)
                    this.$router.push({
                        name: "Login",
                    });
                })
                .catch((err: any) => {
                    this.hasError = true;
                    this.showAlert = true; 
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        this.errorMessages =
                            err.response.data.errors[field[0]][0];
                    } else {
                        this.errorMessages = err.response.data.message;
                    }
                    this.isLoading = false;
                })
                .finally(function() {
                    that.isLoading = false;
                });
        }
    }

    get emailErrors() {
        const errors: Array<string> = [];
        if (!this.$v.email.$dirty) return errors;
        !this.$v.email.email && errors.push("Must be valid e-mail");
        !this.$v.email.required && errors.push("E-mail is required");
        return errors;
    }
}
