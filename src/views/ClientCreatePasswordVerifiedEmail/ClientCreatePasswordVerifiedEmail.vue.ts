import { Component, Prop, Vue } from 'vue-property-decorator';
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import store from "../../store";
import CryptoJsAesJson from '@/CryptoJsAesJson';

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        confirm_password: {
            required,
            minLength: minLength(6),
            sameAs: sameAs("password")
        }
    },
})
export default class ClientCreatePasswordVerifiedEmail extends Vue {

    isLoading: boolean = false;
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";

    password: string = "";
    confirm_password: string = "";
    loginRedirectUrl: string = "https://consumer.gethazina.com";


    submit() {
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let sortParam = this.$route.params.id;


            let data = {
                password: this.password,
                user_id: this.$route.params.id
            };
            let that = this;

            axios 
                .post("/consumer/auth/create-password", data)
                .then(resp => { 
                    this.isLoading = false;
                    this.showAlert = true;
                    this.$toasted.success("You password has been updated successfully!")
                    store.dispatch("token_login", resp.data.token).then(() => {
                        ///this.$router.push({name: 'ConsumerHome'});
                        window.location.href = that.loginRedirectUrl+'/auth/'+resp.data.id;
                    });
                })
                .catch(err => {
                    this.hasError = true;
                    this.showAlert = true;
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        this.errorMessages =
                            err.response.data.errors[field[0]][0];
                    } else {
                        this.errorMessages = err.response.data.errors;
                    }
                })
                .finally(function() {
                    that.isLoading = false;
                });
        }
    }

    get passwordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.password.$dirty) return errors;
        !this.$v.password.required && errors.push("Password is required");
        !this.$v.password.minLength &&
            errors.push("Password must be at least 6 characters long");
        //!this.$v.password.sameAs && errors.push('Passwords do not match')
        return errors;
    }
    get confimPasswordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.confirm_password.$dirty) return errors;
        !this.$v.confirm_password.required &&
            errors.push("You must confirm your password.");
        !this.$v.confirm_password.minLength &&
            errors.push("Password must be at least 6 characters long");
        !this.$v.confirm_password.sameAs &&
            errors.push("Passwords do not match");
        return errors;
    }

}
