import { Component, Vue } from "vue-property-decorator";
import Axios from "axios";
import store from "../../store";
import { required } from "vuelidate/lib/validators";
import * as _ from "lodash";
import CodeInput from "vue-verification-code-input";
import CryptoJsAesJson from "@/CryptoJsAesJson";
declare let DuphluxPop: any;

// Route::post('/phone/verify', 'AuthCtrl@verify_phone');
// Route::post('/phone/send-code', 'AuthCtrl@send_code');
// Route::get('/phone/get-number', 'AuthCtrl@get_phone_number');
// Route::post('/phone/call/verify', 'AuthCtrl@call_verify');

@Component({
  components: {
    CodeInput,
  },
  validations: {
    phone: { required },
  },
})
export default class VerifyPhone extends Vue {
  topCountries: Array<string> = ["NG"];
  onlyCountries: Array<string> = ["NG"];
  shouldResendCode: boolean = false;
  isLoading: boolean = false;
  phone: string = "";
  code: string = "";
  errorMessages: string = "";
  hasError: boolean = false;
  isCalling: boolean = false;
  isResending: boolean = false;
  isLoggingIn: boolean = false;
  duplux_token: string = "926bcd0a983338d6e4528c8ce9fcdd6bd027397f";

  SendCodeSmsSent: boolean = false;
  whatsappDisabled: boolean = false;

  timeHandle: any;

  mounted(){
    if(store.state.location == "direct"){ 
      document.getElementById("SendCodeSms")?.classList.add("appHidden");
      document.getElementById("verifyCodeFrame")?.classList.remove("appHidden");
      var duration = 30, //in seconds
            display = document.querySelector("#smsCodeTimer");
          this.startTimer(duration, display, () => {
            document.getElementById("smsCodeTimer")?.classList.add("appHidden");
            document.getElementById("ShowWhatsapPage")?.classList.remove("appHidden");
           // document.getElementById("sendCodeWhatsappFrame")?.classList.remove("appHidden");
          });
    }
  }

  sortSection(section: string, hidden: Array<string>){
    document.getElementById(section)?.classList.remove("appHidden");
    hidden.forEach((value, ind)=>{
      document.getElementById(value)?.classList.add("appHidden");
    });
  }

  SendCodeSms() {
    this.$v.$touch();
    this.isResending = true;
    if (!this.$v.$invalid) {
      this.sendCodeApi()
        .then((resp: any) => {
          this.isResending = false;
          this.$toasted
            .success("A new code has been sent to your phone")
            .goAway(5000);
          document.getElementById("SendCodeSms")?.classList.add("appHidden");
          document.getElementById("verifyCodeFrame")?.classList.remove("appHidden");
          var duration = 30, //in seconds
            display = document.querySelector("#smsCodeTimer");
          this.startTimer(duration, display, () => {
            document.getElementById("smsCodeTimer")?.classList.add("appHidden");
            document.getElementById("ShowCodeCallPage")?.classList.remove("appHidden");
            document.getElementById("ShowWhatsapPage")?.classList.remove("appHidden");
          });
        })
        .catch((error) => {
          this.isResending = false;
          this.$toasted
            .success("There was an error sending a new code")
            .goAway(5000);
        });
    } else {
      this.isResending = false;
      //this.$toasted.error("You must provide a phone phone number").goAway(5000);
    }
  }

  Verify() {
    clearInterval(this.timeHandle);
    this.verifyApi()
      .then((resp: any) => {
        store.dispatch("token_login", resp.token).then(() => {
          this.isLoggingIn = true;
          //window.location.href = this.loginRedirectUrl;
          window.location.href = store.state.loginUrl + resp.id;
        });
      })
      .catch((err) => {
        this.errorMessages = !err ? "" : err.response.data.errors;
      });
  }

  verifyApi() {
    this.isLoading = true;
    return new Promise((resolve, reject) => {
      Axios.post("/consumer/auth/phone/verify", {
        id: this.$route.params.id,
        code: this.code,
      })
        .then((resp) => {
          this.isLoading = false;
          resolve({ token: resp.data.token, id: resp.data.id });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  sendCodeApi(phone: string = "") {
    return new Promise((resolve, reject) => {
      Axios.post("/consumer/auth/phone/send-code", {
        id: this.$route.params.id,
        phone: phone.replace(/\s+/g, ""),
      })
        .then((resp) => {
          resolve({ data: resp.data.data });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  startTimer(duration: number, display: any, callback: () => any) {
    var t = new Date();
    var countDownDate = t.setSeconds(t.getSeconds() + duration);
    var timeHandle = this.timeHandle;
    timeHandle = setInterval(function() {
      var now = new Date().getTime();
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      display.textContent = "Try another way to verify in " + minutes + ":" + seconds;
      if (distance < 0) {
        clearInterval(timeHandle);
        display.textContent = "";
        callback();
      }
    });
  }

  SendWhatsapp() {
    Axios.post("/consumer/auth/phone/whatsapp", { id: this.$route.params.id })
      .then((resp: any) => {
        this.$toasted
          .success("Verification code sent to your WhatsApp")
          .goAway(500);
        this.whatsappDisabled = true;
        var duration = 60, //in seconds
          display = document.querySelector("#whatsappCodeTimer");
        this.startTimer(duration, display, () => {
          // document.getElementById("sendCodeWhatsappFrame")?.classList.add("appHidden");
          // document.getElementById("sendCodeCallFrame")?.classList.remove("appHidden");
          document.getElementById("whatsappCodeTimer")?.classList.add("appHidden");
            document.getElementById("ShowCodeCallPage")?.classList.remove("appHidden");
        });
      })
      .catch((err) => {
        this.whatsappDisabled = true;
        this.$toasted.error("Error Sending Whatsapp Message").goAway(500);
      });
  }

  callVerify() {
    this.isCalling = true;
    Axios.get("/consumer/auth/phone/get-number", {
      params: { id: this.$route.params.id },
    })
      .then((resp: any) => {
        //this.isCalling = false;
        this.initPhoneCall(resp.data.phone, resp.data.reference);
      })
      .catch((err) => {
        this.isCalling = false;
        this.errorMessages = !err ? "" : err.response.data.errors;
        //console.log(err);
      });
  }

  initPhoneCall(phone: String, reference: String) {
    let that = this;
    (<any>DuphluxPop).init({
      token: this.duplux_token,
      timeout: 900,
      phone_number: phone,
      redirect_url:
        process.env.NODE_ENV === "production"
          ? window.location.href
          : "https://127.0.0.1:8080",
      transaction_reference: reference,
      callback: () => {
        return {
          onSuccess: function(reference: any) {
            Axios.post("/consumer/auth/phone/call/verify", {
              id: that.$route.params.id,
            })
              .then((resp) => {
                that.isCalling = false;
                store.dispatch("token_login", resp.data.token).then(() => {
                  window.location.href = store.state.loginUrl + resp.data.id;
                });
              })
              .catch((err) => {
                that.isCalling = false;
                that.errorMessages = err.response.data.errors;
              });
          },
          onFailure: function(error: any) {
            that.isCalling = false;
            that.errorMessages =
              "Unable to verify your phone number, please try again";
          },
          onError: function(errorMessage: any) {
            that.isCalling = false;
            that.errorMessages =
              "Unable to verify your phone number, please try again";
          },
        };
      },
    });
    (<any>DuphluxPop).launch();
  }

  setPhone(evt: any) {
    this.code = evt;
  }

  sendCode() {
    this.isResending = true;
    this.sendCodeApi()
      .then((resp: any) => {
        this.isResending = false;
        this.$toasted
          .success("A new code has been sent to your phone")
          .goAway(5000);
      })
      .catch((error) => {
        this.isResending = false;
        this.$toasted
          .success("There was an error sending a new code")
          .goAway(5000);
      });
  }

  sendCodeNewNumber() {
    this.isLoading = true;
    this.sendCodeApi(this.phone)
      .then((resp: any) => {
        this.shouldResendCode = false;
        this.isLoading = false;
      })
      .catch((err) => {
        this.hasError = true;
        this.isLoading = false;
        if (
          _.has(err.response.data, "errors") &&
          typeof err.response.data.errors != "string"
        ) {
          var field = _.keys(err.response.data.errors);
          this.errorMessages = err.response.data.errors[field[0]][0];
        } else {
          this.errorMessages = err.response.data.errors;
        }
      });
  }

  get phoneErrors() {
    const errors: Array<string> = [];
    if (!this.$v.phone.$dirty) return errors;
    !this.$v.phone.required &&
      errors.push("You must provide your phone number.");
    return errors;
  }
}
