import Axios from 'axios';
import { Component, Vue } from "vue-property-decorator";
import Checkmark from '../Checkmark/Checkmark.vue';
import store from "@/store/index";

@Component({
    components:{
        
    }
})
export default class Thanks extends Vue {
    colors: any =  [
        'indigo',
        'warning',
        'pink darken-2',
        'red lighten-1',
        'deep-purple accent-4',
      ]

      slides: any = [
        'First',
        'Second',
        'Third',
        'Fourth',
        'Fifth',
      ]

      isLoading: boolean = false;

      campaign: any = {};

      dialog: any = false;
      dialogType: any = "ACCEPT";

      AcceptOfferConfirm(){
        if(!this.isLoggedIn){
            this.dialog = true;
            this.dialogType = "LOGIN";
        }else{
          this.dialog = true;
          this.dialogType = "ACCEPT";
        }
      }

      AcceptOffer(){ 
        this.dialog = false;
          this.isLoading = true;
          Axios.post("/consumer/accept-offer/"+this.$route.params.id)
          .then((resp)=>{
              this.isLoading = false;
              this.$toasted.success("Campaign has been successfully added to your offers").goAway(1000)
              this.$router.push({name: "ConsumerHome"});
          })
          .catch((err)=>{
            this.isLoading = false;
            this.$toasted.error(err.response.data.errors);
          })

      }

      loadOffer(){
        Axios.get("/consumer/campaign/load-offer/"+this.$route.params.id)
        .then((resp)=>{
          this.campaign = resp.data.data;
        })
        .catch((err)=>{
          this.$toasted.error("Error loading Campaign").goAway(1000)
          this.$router.push({name: "Home"});
        })
      }

      get isLoggedIn() {
        return store.getters.isLoggedIn;
    }


      mounted(){ 
        this.loadOffer();
      }

}
