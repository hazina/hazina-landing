import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import store from "@/store";

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        confirm_password: {
            required,
            minLength: minLength(6),
            sameAs: sameAs("password")
        },
        email: { required, email },
        phone: { required }
    }, 
    components:{
    }
})
export default class Register extends Vue {
    isLoading: boolean = false;
    hasError: boolean = false;
    showAlert: boolean = false;
    errorMessages: string = "";

    topCountries: Array<string> = [
        'NG'
    ]
    onlyCountries: any = ["NG"];

    //username: string = ""; 
    email: string = ""; 
    password: string = "";
    confirm_password: string = "";
    phone: string = "";

    submit() {
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let data = {
                email: this.email,
                phone: this.phone.replace(/\s+/g, ''),
                //username: this.username,
                password: this.password,
                ref_code: this.$route.query.ref
            };
            let that = this;

            axios
                .post("/consumer/auth/register", data)
                .then(res => {
                    this.isLoading = false;
                    this.showAlert = true;
                    store.commit("verify_referrer", "direct");
                    this.$router.push({
                        name: "VerifyPhone",
                        params: { id: res.data.data.id }
                    });

                })
                .catch(err => {
                    console.log(err);
                    this.hasError = true;
                    this.showAlert = true;
                    if (
                        _.has(err.response.data, "errors") &&
                        typeof err.response.data.errors != "string"
                    ) {
                        var field = _.keys(err.response.data.errors);
                        this.errorMessages =
                            err.response.data.errors[field[0]][0];
                    } else {
                        this.errorMessages = err.response.data.errors;
                    }
                })
                .finally(function() {
                    that.isLoading = false;
                });
        }
    }

    // get usernameErrors() {
    //     const errors: Array<string> = [];
    //     if (!this.$v.username.$dirty) return errors;
    //     !this.$v.username.minLength &&
    //         errors.push("Username must be at least 4 characters long");
    //     !this.$v.username.required && errors.push("Username is required.");
    //     return errors;
    // }

    get emailErrors() {
        const errors: Array<string> = [];
        if (!this.$v.email.$dirty) return errors;
        !this.$v.email.email && errors.push("Must be valid e-mail");
        !this.$v.email.required && errors.push("E-mail is required");
        return errors;
    }
    get passwordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.password.$dirty) return errors;
        !this.$v.password.required && errors.push("Password is required");
        !this.$v.password.minLength &&
            errors.push("Password must be at least 4 characters long");
        //!this.$v.password.sameAs && errors.push('Passwords do not match')
        return errors;
    }
    get confimPasswordErrors() {
        const errors: Array<string> = [];
        if (!this.$v.confirm_password.$dirty) return errors;
        !this.$v.confirm_password.required &&
            errors.push("You must confirm your password.");
        !this.$v.confirm_password.minLength &&
            errors.push("Password must be at least 4 characters long");
        !this.$v.confirm_password.sameAs &&
            errors.push("Passwords do not match");
        return errors;
    }

    get phoneErrors() {
        const errors: Array<string> = [];
        if (!this.$v.phone.$dirty) return errors;
        !this.$v.phone.required &&
            errors.push("You must provide your phone number.");
        return errors;
    }
}
