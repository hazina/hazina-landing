import { Component, Prop, Vue } from "vue-property-decorator";
import axios from 'axios';
import Checkmark from '../Checkmark/Checkmark.vue';
import Spinner from '../Spinner/Spinner.vue';
import store from '@/store'
@Component({
    components:{
        Checkmark,
        Spinner
    }
})
export default class PhoneCodeVerify extends Vue {
    result = "";
    emailMsg = "";
    hasError = false;
    isSuccess = false;
    isVerified = false;
    userID: any = "";

    verifyPhone() {
        let data = {
            code: this.$route.params.code
        }; 
        let that = this;

        axios
            .post("/consumer/auth/phone-code-verify", data)
            .then(resp => {
                this.hasError = false;
                this.isSuccess = true;
                this.isVerified = true;
                this.result = "success";
                this.userID = resp.data.id
            })
            .catch(err => {
                this.hasError = true;
                this.isSuccess = false;
                this.isVerified = true;
                this.emailMsg = err.response.data.errors;
            })
            .finally(function() {
                //that.isLoading = false;
            });
    }

    mounted() {
        this.verifyPhone();
    }

    login(){
        window.location.href = store.state.loginUrl + this.userID;
    }
}
