declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "vue-tel-input-vuetify/lib"
declare module "vue-verification-code-input"
declare module "vuetify-google-autocomplete" 
declare module "survey-knockout"
declare module "vue-gtm"