import CryptoJs from "crypto-js"

export default class CryptoJsAesJson{

    // public EncPwd: string = 'EaEenw5hgYnn8jQmnaVBTzv38DVbVTyncHZ2t7TebMy5n7C2YHcnX7ajGwdUK3RFVPtGgdqTbevwHgfP98gPstcyNQg4qGbvcCFrYDUJbsdC8z';

    Encrypt(data: string){
        let EncPwd = 'EaEenw5hgYnn8jQmnaVBTzv38DVbVTyncHZ2t7TebMy5n7C2YHcnX7ajGwdUK3RFVPtGgdqTbevwHgfP98gPstcyNQg4qGbvcCFrYDUJbsdC8z';
        var encrypted = CryptoJs.AES.encrypt(data.toString(), EncPwd, { 
            mode: CryptoJs.mode.ECB 
        });
        return encrypted.toString();
      }
    
      Decrypt(hash: string){
        let EncPwd = 'EaEenw5hgYnn8jQmnaVBTzv38DVbVTyncHZ2t7TebMy5n7C2YHcnX7ajGwdUK3RFVPtGgdqTbevwHgfP98gPstcyNQg4qGbvcCFrYDUJbsdC8z';
        var decrypted = CryptoJs.AES.decrypt(hash, EncPwd, { 
            mode: CryptoJs.mode.ECB, 
        });
        return decrypted.toString(CryptoJs.enc.Utf8);
      } 

       enc(plainText: string){
        let EncPwd = 'EaEenw5hgYnn8jQmnaVBTzv38DVbVTyncHZ2t7TebMy5n7C2YHcnX7ajGwdUK3RFVPtGgdqTbevwHgfP98gPstcyNQg4qGbvcCFrYDUJbsdC8z';
        var b64 = CryptoJs.AES.encrypt(plainText, EncPwd).toString();
        var e64 = CryptoJs.enc.Base64.parse(b64);
        var eHex = e64.toString(CryptoJs.enc.Hex);
        return eHex;
    }
    
     dec(cipherText:string){
      let EncPwd = 'EaEenw5hgYnn8jQmnaVBTzv38DVbVTyncHZ2t7TebMy5n7C2YHcnX7ajGwdUK3RFVPtGgdqTbevwHgfP98gPstcyNQg4qGbvcCFrYDUJbsdC8z';
       var reb64 = CryptoJs.enc.Hex.parse(cipherText);
       var bytes = reb64.toString(CryptoJs.enc.Base64);
       var decrypt = CryptoJs.AES.decrypt(bytes, EncPwd);
       var plain = decrypt.toString(CryptoJs.enc.Utf8);
       return plain;
    }

 
}
