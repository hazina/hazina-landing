module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  // publicPath: "/portals/app/",
  pwa:{
    name: 'Hazina',
    themeColor: '#f5a72b',
    msTileColor: '#ffffff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  }
}